#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Stian Rødven Eide

import sys
import os
import re
import subprocess
import taglib
import shutil
import parsecue
from fixenglish import fix_english as fe
#import chardet

# Todo
# * Does not care about pregaps. Should it?
# * pretty up sys.argvs 
# * FLAC renaming scheme could be an optional argument input
# * Turn logging on/off could be an optional argument input
# * Add option to move cue/log to Logs dir
# * Try to replace shntool. Pydub?
# * Cue files should be converted to UTF-8 before reading
# * I found several cases of disc order not being guessed correctly


def flacrename(flacfile):
    """Renames a flac file based on its metadata.
       Will substitute suboptimal characters such as '/'.
       A KeyError is raised if metadata doesn't exist.
    """
    audio = taglib.File(flacfile)
    tracknumber = audio.tags["TRACKNUMBER"][0]
    title = audio.tags["TITLE"][0]
    if len(tracknumber) == 1:
        tracknumber = "0" + tracknumber
    newname = "%s - %s.flac" % (tracknumber, title)
    newname = re.sub('/', '-', newname)
    newname = re.sub(':', ' -', newname)
    newname = re.sub('\?', '', newname)
    if "DISCNUMBER" in audio.tags:
        disc = audio.tags["DISCNUMBER"][0]
        newname = "Disc %s - %s" % (disc, newname)
    print(flacfile + " > " + newname)
    os.rename(flacfile, newname)


def splitter(cuesheet, flacfile, disc):
    """Takes a cuesheet and an audio file. Splits the audio file
       based on information in the cuesheet. Sends the split files
       further to tagging and renaming.
    """
    alen = len(cuesheet.tracks)
    if alen > 1:
        # Split it!
        print("\nSplitting big flac")
        os.system('echo "%s" | shnsplit -o flac "%s"' \
                  % (cuesheet.cuebreakpoints(), flacfile))
        # Check that we have the right number of split tracks
        print("\nChecking number of new files:", end=" ")
        newflacs = [f for f in os.listdir(".") if 
                    f.startswith("split-track") and f.endswith(".flac")]
        newflacs.sort()
        if len(newflacs) == alen:
            # The split was probably successful and we 
            # can remove the original flac file
            print("OK")
            print("\nRemoving big flac")
            os.system('rm "' + flacfile + '"')
        else:
            # This should not happen
            print("\nNumber of tracks doesn't correspond to cue file. \
                  Leaving big flac alone.")

        # Tag the split tracks with metadata from the cue file
        print("\nTagging new flacs")
        for i,flac in enumerate(newflacs):
            tagflac(disc, i+1, flac, cuesheet)

        # Rename split tracks
        print("\nWriting filenames from tags:")
        for f in sorted(newflacs):
            try:
                flacrename(f)
            except KeyError:
                print("Missing metadata for %s!" % f)
                
    elif alen == 1:
        # The flac doesn't need to be split
        print("\nOnly one song here it seems.. tagging that")
        tagflac(disc, 1, flacfile, cuesheet)


def tagflac(disc, number, flac, cuesheet):
    """Takes as input discnumber, track number, flac file and cuesheet
       object. Tags the flac file with all present metadata.
    """
    audio = taglib.File(flac)
    alen = len(cuesheet.tracks)
    album_performer = fe(cuesheet.performer)
    album_title = fe(cuesheet.title)
    album_date = cuesheet.date
    album_genre = fe(cuesheet.genre)
    album_comment = cuesheet.comment
    # Remember track info as ti
    ti = cuesheet.track(number)
    # Remember track number string as tn with leading zero for 1-9
    if len(str(number)) == 1:
        tn = "0" + str(number)
    else:
        tn = str(number)
    try:
        audio.tags["ARTIST"] = [fe(ti["PERFORMER"])]
        if ti["PERFORMER"] != album_performer:
            audio.tags["ALBUMARTIST"] = [album_performer]
    except KeyError:
        audio.tags["ARTIST"] = [album_performer]
    audio.tags["ALBUM"] = [album_title]
    audio.tags["DATE"] = [album_date]
    audio.tags["GENRE"] = [album_genre]
    audio.tags["COMMENT"] = [album_comment]
    audio.tags["TRACKNUMBER"] = [tn]
    audio.tags["TRACKTOTAL"] = [str(alen)]
    audio.tags["TITLE"] = [fe(ti["TITLE"])]
    # Add a tag for disc number if more than one disc exists
    if disc > 0:
        audio.tags["DISCNUMBER"] = [str(disc)]
    retval = audio.save()
    retval


def convert_audio(audiofile):
    """Takes an ape or wv file name as input and converts it to flac. 
       Deletes the original and returns the flac file name if 
       successful. Raises a FileNotFoundError if unsuccessful.
    """
    # In case neither ffmpeg nor avconv is present
    if not converter:
        print("\nAn ape or wv file was found, but there is " \
              "no conversion software present.")
        raise FileNotFoundError("FLAC Conversion failed!")    
    if audiofile.lower().endswith(".ape"):
        filetype = "ape"
        basename = audiofile[:-4]
    elif audiofile.lower().endswith(".wv"):
        filetype = "wv"
        basename = audiofile[:-3]
    print("\nConverting %s to flac" % filetype)
    try: 
        os.system('%s -i "%s" "%s.flac"' \
                  % (converter, audiofile, basename))
        os.system('rm "%s"' % audiofile)
        flacfile = basename + '.flac'
        return flacfile
    # Not sure if OSError is the one that would happen
    except OSError:
        print("\nFailed to convert ape file..")
        raise FileNotFoundError("FLAC Conversion failed!")

    
def renameonly(directory):
    """In case we have split the tracks already.
       With a small modification it could rename all
       flacs using a predefined scheme.
    """
    for root, dirs, files in os.walk(directory):
        os.chdir(root)
        for f in files:
            if f.startswith("split-track"):
                try:
                    flacrename(f)
                except KeyError:
                    print("Missing metadata for %s!" % f)


def current(directory):
    """Takes a directory as input and defines what goes on there.
       1. Make a list of cue- and audio files
       2. Try to match each audio file to a cue
       3. Send cue/audio pair to splitter function
       4. Abort on the first failure to match
       Currently returns None.
    """
       
    def process_audio(audiofile, disc=0):
        """Performs the cue matching and sends the appropriate
           pair to the splitter function.
        """
        # We parse the file name with regex. It should handle
        # any scenario with a 2, 3 or 4 letter file extension.
        # However, we should probably have some code for a No Match
        parsename = re.match("(.+?)(\.+)(.{2,4}$)", audiofile)
        # Group 1 is everything before the last dot
        basename = parsename.group(1)
        # Group 3 is everything after the last dot
        filetype = parsename.group(3)
        # We expect the cue file to have the same basename
        # as the audio file, potentially plus the audio file's
        # file extension
        if audiofile + ".cue" in cues:
            cuefile = audiofile + ".cue"
        elif audiofile + ".CUE" in cues:
            cuefile = audiofile + ".CUE"
        elif basename + ".cue" in cues:
            cuefile = basename + ".cue"
        elif basename + ".CUE" in cues:
            cuefile = basename + ".CUE"
        else:
            # This will happen if the directory 
            # already has been processed.
            print("\nCannot find matching cue file!")
            raise FileNotFoundError("Cannot find matching cue file!")

        # Lastly, we check that the cue file can be read
        print("\nChecking whether cue file is parseable:", end=" ")
        try:
            cuesheet = parsecue.CueSheet()
            f = open(cuefile).read()
            cuesheet.parse_cue(f)
            cuesheet.test
            print("OK")
            if filetype == "ape" or (filetype == 'wv' and not wavpack):
                # Since mac is nonfree, we don't expect it to be 
                # installed and rather convert any ape file to flac.
                # Wv files will be converted if wavpack is not installed
                try:
                    audiofile = convert_audio(audiofile)
                except FileNotFoundError:
                    print("FLAC Conversion failed!")
                    return
            # Send to splitter!
            splitter(cuesheet, audiofile, disc)
        except UnicodeDecodeError:
            print("NO!")
            print("Unable to parse cue file:", end=" ")
            # Todo: store all of these in a global variable
            # and return the list at the end of the program
            print(cuefile)

    print("\nProcessing directory: %s" % directory)
    # Let the current path be the working directory
    os.chdir(directory)
    cues = []
    audiofiles = []
    # f should be lowered()
    for f in os.listdir(directory):
        # Make lists of cue- and audio files
        if os.path.isdir(f):
            continue
        if f.lower().endswith(".flac"):
            audiofiles.append(f)
        if f.lower().endswith(".ape"):
            audiofiles.append(f)
        if f.lower().endswith(".wav"):
            audiofiles.append(f)
        if f.lower().endswith(".wv"):
            audiofiles.append(f)
        if f.lower().endswith(".cue"):
            cues.append(f)
        
    if len(audiofiles) == 0:
        # Nothing to do here. Carry on.
        print("\nNo audio files in directory!")
        return
    elif len(audiofiles) == 1:
        # The simple scenario
        audiofile = audiofiles[0]
        try:
            process_audio(audiofile)
        except FileNotFoundError:
            return
    elif len(audiofiles) > 1:
        # For crowded directories.
        # To avoid unneccesary tries, we abort
        # on the first failure to match
        count = 0
        for audiofile in sorted(audiofiles):
            try:
                # Here we also send the disc number for tagging and 
                # rename purposes. These are currently guessed based 
                # on alphabetical order.
                count += 1
                process_audio(audiofile, disc=count)
            except FileNotFoundError:
                return


def traverse(directory):
    """This function walks through all subdirectories, processing
       them in turn.
    """
    for root, dirs, files in os.walk(directory):
        current(root)


def check_dependencies():
    """Checks that the needed tools are present. Depends currently
       on 'shntool' and recommends either 'ffmpeg' or 'libav-tools'. 
       Wavpack is also a recommended dependency.
       Prints out what is missing and returns a tuple with
       - True/False for shntool
       - The name of the converter, or None if none
       - True/False for wavpack
    """
    if shutil.which("shnsplit"):
        split_tools = True
    else:
        if not shutil.which("shnsplit"):
            print("'shnsplit' is missing. Install it with " \
                  "'sudo apt-get install shntool'.")
        split_tools = False

    if not shutil.which("ffmpeg") and not shutil.which("avconv"):
        print("If you want to process ape files, " \
              "you need either 'ffmpeg' or 'avconv'.")
        print("Try 'sudo apt-get install ffmpeg' or")
        print("'sudo apt-get install libav-tools'.")
        converter = None
    else:
        if shutil.which("ffmpeg"):
            converter = "ffmpeg"
        if shutil.which("avconv"):
            converter = "avconv"

    if shutil.which("wvunpack"):
        wavpack = True
    else:
        if not converter:
            print("If you want to process wv files, you need " \
                  "either 'wavpack' or a program to convert them, " \
                  "i.e. 'ffmpeg' or 'avconv'. You can try 'sudo " \
                  "apt-get install wavpack.")
        else:
            print("Wavpack was not found. Wv files will be " \
                  "converted if found. You can avoid this by " \
                  "installing wavpack with 'sudo apt-get install " \
                  "wavpack'.")
        wavpack = False

    return (split_tools, converter, wavpack)


if __name__ == "__main__":
    # First we check for dependencies
    print("Checking dependencies:", end=" ")
    split_tools, converter, wavpack = check_dependencies()
    if not split_tools:
        raise FileNotFoundError("Basic dependencies missing.")
    else:
        print("OK!")
    if converter:
        print("Using %s for conversion purposes." % converter)
    else:
        print("Ape conversion is disabled")
    wait = input("\nPress Enter to continue...")

    # Then we parse the arguments        
    arguments = sys.argv[1:]
    validflags = ["-current", "-full", "-rename"]
    flag = ''
    directory = ''

    # Here we determine the starting path
    if len(arguments) == 1:
        # We assume the cwd if only one argument was passed
        flag = arguments[0]
        directory = os.getcwd()
    elif len(arguments) == 2:
        # Else the second argument is assumed to be the path
        flag = arguments[0]
        # We must still check that the path is valid
        if os.path.isdir(arguments[1]):
            directory = arguments[1]
        else:
            print("Invalid path")
    
    if flag in validflags and directory:
        # We have both a valid flag and a path
        if arguments[0] == "-full":
            print("Doing the full walk")
            traverse(directory)
        elif arguments[0] == "-rename":
            renameonly(directory)
        elif arguments[0] == "-current":
            current(directory)
    else:
        # User error
        print("Suboptimal syntax")
        print("Usage: splitflac [-flag] (path)")
        print("where [-flag] is one of")
        print("    -current")
        print("    -full")
        print("    -rename")
        print("and path is optional")
